#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "Queue.h"

// A queue is composed of two stacks.  The outgoing stack is where values
// are popped off from, and the incoming stack is where new values are
// initially pushed.
struct queue {
	Stack incoming;
	Stack outgoing;
};

// 'flips' the 'source' Stack onto the 'dest' Stack by popping an item off
// of 'source' and pushing it onto 'dest' until 'source' is empty.
int flipS( Stack *dest, Stack *source )
{
	if( isEmptyS( source ) )
		return false;

	char *str;
	while( !isEmptyS( source ) ) {
		popS( source, &str );
		pushS( dest, str );
	}

	return true;
}

int createQ( Queue *que )
{
	struct queue *tmp = malloc( sizeof(struct queue) );

	createS( &(tmp->incoming) );
	createS( &(tmp->outgoing) );

	*que = tmp;
	return true;
}

int isEmptyQ( Queue *que )
{
	// Check to see if both stacks are empty.
	if( isEmptyS( &((*que)->incoming) ) && isEmptyS( &((*que)->outgoing) ) )
		return true;

	return false;
}

int pushQ( Queue *que, char *s )
{
	if( pushS( &((*que)->incoming), s ) )
		return true;

	return false;
}

int popQ( Queue *que, char **s )
{
	if( isEmptyQ( que ) )
		return false;

	// If the outgoing stack is empty, flip the incoming stack onto the
	// outgoing stack.
	if( isEmptyS( &((*que)->outgoing) ) )
		flipS( &((*que)->outgoing), &((*que)->incoming) );

	if( !popS( &((*que)->outgoing), s ) )
		return false;

	return true;
}

int destroyQ( Queue *que )
{
	if( !(destroyS( &((*que)->incoming) ) && destroyS( &((*que)->incoming) )) )
		return false;

	return true;
}