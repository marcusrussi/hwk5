#include "Stack.h"

typedef struct queue *Queue;

/**
 * Creates an empty Queue.
 * @param  que Pointer to unitialized Queue.
 * @return     'true' on success, 'false' on failure.
 */
int createQ( Queue *que );

/**
 * Checks if given Queue is empty.
 * @param  que Pointer to Queue.
 * @return     'true' if empty, 'false' if nonempty.
 */
int isEmptyQ( Queue *que );

/**
 * Pushes the string 's' into the back of the Queue 'que'
 * @param  que The queue to push the string 's' into.
 * @param  s   The string to be pushed.
 * @return     'true' on success, 'false' on failure.
 */
int pushQ( Queue *que, char *s );

/**
 * Pops the string at the top of the queue off and assigns 's' to point to it
 * @param  que The queue to operate on.
 * @param  s   The character pointer to pop the topmost string off onto.
 * @return     'true' on success, 'false' on failure.
 */
int popQ( Queue *que, char **s );

/**
 * Destroys the given queue.
 * @param  que The queue to destroy.
 * @return     'true' on success, 'false' on failure.
 */
int destroyQ( Queue *que );
