typedef struct trie *Trie;

/**
 * Creates a blank Trie data structure.
 * @param  t Pointer to a 'Trie'.
 * @return   'true' on success, 'false' on failure.
 */
int createT( Trie *t );

/**
 * Inserts 'pos' into the Trie, along with its associated 'from'
 * and 'len' values.
 * @param  t    The Trie to insert into.
 * @param  pos  The string to be inserted.
 * @param  from The string from which pos was generated from.
 * @param  len  The number of moves used to get to 'pos'.
 * @return      'true' on success, 'false' on failure.
 */
int insertT( Trie *t, char *pos, char *from, int len );

/**
 * Searches for 'pos' in Trie 't'.
 * @param  t    The Trie to search.
 * @param  pos  The string to search for.
 * @param  from Pointer to hold the 'from' string, should it be found.
 * @param  len  Pointer to hold the 'len' integer, should it be found.
 * @return      'true' when found, 'false' when not found.
 */
int searchT( Trie *t, char *pos, char **from, int *len );
