#ifndef _utils_h
#define _utils_h

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "dbg.h"
#include "Trie.h"

#define RIGHT 1
#define DOWN 2

/**
 * Prints a human-readable Rubik's cube.
 * @param str    The null-terminated string containing the representation
 *               of the cube.
 * @param height The number of units of height for the cube 'tray'.
 * @param width  The number of units of width for the cube 'tray'.
 */
void printRubik( char *str, int height, int width );

/**
 * Returns an array of strings corresponding to all of the possible
 * permutaitons of a given Rubik's cube representation, in 'str'
 * @param  str    The initial Rubik's cube string
 * @param  nperms Pointer to number of permutations.
 * @param  rflag  1 when '-r' is specified, 0 otherwise
 * @param  h      Height of cube.
 * @param  w      Width of cube.
 * @return        NULL on failure.
 */
char **allPermutations( char *str, int *nperms, int rflag, int h, int w );

/**
 * Given a string representing a Rubik's Cube, rotates the cube in a specified
 * direction.
 * @param  str       The string representing the Rubik's Cube.
 * @param  direction The direction in which to shift.  RIGHT or DOWN 
 *                   are supported.
 * @param  distance  The number of units to shift.  Must be a positive integer.
 * @param  whichline Which column or row to shfit.
 * @param  h         The height of the Rubik's Cube.
 * @param  w         The width of the Cube.
 * @return           NULL pointer on failure, resulting string on success.
 */
char *shift( char *str, int direction, int distance, int whichline, \
			int h, int w );

/**
 * Given a Trie and an initial string, follows the 'from' strings until
 * it reaches the empty string.
 * @param initial    The initial string to find in the trie.
 * @param dictionary The trie.
 */
void printSolution( char *initial, Trie dictionary );

#endif
