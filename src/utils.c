#include "utils.h"
#define strEquals(a, b) (strcmp( (a), (b) ) == 0)


void printRubik( char *str, int height, int width )
{
	int l;
	check( (l = strlen( str )) == height * width, \
		"The string length (%d) must match the height (%d) and width (%d).", \
		l, height, width );

	for( int i=0; i < l; i++ ) {
		if ( i % width == 0 && i != 0)
			printf("\n");
		printf( "%c", str[i] );
	}
	printf("\n\n");

	return;

error:
	log_err( "'printRubik()' failed." );
	return;
}

char **allPermutations( char *str, int *nperms, int rflag, int h, int w )
{
	check( rflag == 0 || rflag == 1, "'rflag' must be set to 0 or 1." );

	char **result;
	if( rflag ) {
		result = malloc( ( (w-1)*h + (h-1)*w ) * sizeof(char *) );
		int n = 0;
		for( int i=0; i < w; i++ ) {
			for( int j=1; j < h; j++, n++ ) {
				result[n] = shift( str, DOWN, j, i, h, w);
			}
		}

		for( int i = 0; i < h; i++ ) {
			for( int j=1; j < w; j++, n++ ) {
				result[n] = shift( str, RIGHT, j, i, h, w);
			}
		}

		*nperms = n;
	}
	else {
		result = malloc( (h + w) * sizeof(char *) );

		int i;
		for( i = 0; i < w; i++ ) {
			result[i] = shift( str, DOWN, 1, i, h, w  );
		}
		int j;
		for( j = 0; j < h; j++ ) {
			result[i] = shift( str, RIGHT, 1, j, h, w );
			i++;
		}

		*nperms = i;
	}

	return result;

error:
	return NULL;
}

char *shift( char *in, int dir, int dst, int whichline, \
			int h, int w )
{
	char *str = malloc( (strlen(in) + 1) * sizeof(char) );
	strcpy( str, in );

	check( dst > 0, "Distance shifted must be an int > 0." );
	check( whichline >= 0, \
		"You may not specify a row or column using a negative integer." );
	check( dir == RIGHT || dir == DOWN,\
		"'direction' must be specified as RIGHT or DOWN." );

	if( dir == RIGHT ) {
		check( whichline < h, "Row specified was outside bounds of the cube." );
	}
	else {
		check( whichline < w, "Column specified was outside cube boundaries." );
	}

	int index, shift, max;
	if( dir == RIGHT ) {
		index = w * whichline;
		shift = 1;
		max = w * (whichline + 1);
	}
	else if( dir == DOWN ) {
		index = whichline;
		shift = w;
		max = w * h;
	}

	for( int i=0; i < dst; i++ ) {

		char replacer = str[index];
		char target_c;

		for( int j=index+shift; j < max && j < h*w; j += shift ) {
			target_c = str[j];
			str[j] = replacer;
			replacer = target_c;
		}

		str[index] = replacer;
	}

	return str;
 
error:
	free( str );
	return NULL;
}

void printSolution( char *initial, Trie dictionary )
{
	char *output = initial;
	int r, l;

	while( !strEquals( output, "" ) ) {
		printf( "%s\n", output );
		r = searchT( &dictionary, output, &output, &l );
		check( r, "Trie search for '%s' failed.", output );
	}

	return;

error:
	return;
}
