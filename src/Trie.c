#include <stdlib.h>
#include <stdbool.h>
#include "dbg.h"
#include "Trie.h"

// 'nodes' is an array of pointers to 'trie' structs.
struct trie {
	struct trie **nodes;
	char *from;
	int length;
};

/**
 * Allocates storage for a new 'trie' struct, and initializes the 'nodes'
 * array to point to 12 further 'trie' structs, which are initially NULL.
 * By default, the 'from' value is NULL and the 'length' value is -1.
 * @return Valid pointer on success, NULL pointer on failure.
 */
struct trie *createTrieNode()
{
	struct trie *tmp = malloc( sizeof(struct trie) );
	check_mem( tmp );

	// Allocate space for 12 pointers, set each to NULL.
	tmp->nodes = malloc( 12 * sizeof(struct trie *) );
	check_mem( tmp->nodes );
	for( int i=0; i < 12; i++ )
		tmp->nodes[i] = NULL;

	// Default values.
	tmp->from = NULL;
	tmp->length = -1;

	return tmp;

error:
	if( tmp->nodes ) free( tmp->nodes );
	if( tmp ) free( tmp );
	return NULL;
}

int createT( Trie *t )
{
	*t = createTrieNode();
	check_mem( *t );

	return true;

error:
	return false;
}

int insertT( Trie *t, char *pos, char *from, int len )
{
	struct trie *next = *t;
	int i, j;

	// For each character, search for a corresponding trie node, and 
	// if it doesn't exist, create it and follow it.
	for( i = 0; pos[i] != '\0'; i++ ) {

		// Map our A-L character to an interger indice.
		j = pos[i] - 'A';

		if( next->nodes[j] == NULL )
			next = (next->nodes[j] = createTrieNode());
		else
			next = next->nodes[j];

	}

	// Upon reaching the terminating node, assign the 'from' and 'len' values.
	next->from = from;
	next->length = len;

	return true;
}

int searchT( Trie *t, char *pos, char **from, int *len )
{
	struct trie *next = *t;
	int i, j;
	for( i = 0; pos[i] != '\0' && next != NULL; i++ ) {
		j = pos[i] - 'A';
		next = next->nodes[j];
	}

	// Check to see if the current node is indeed a terminating node.
	if( pos[i] == '\0' && next->from != NULL && next->length != -1 ) {

		// Assign search results.
		*from = next->from;
		*len = next->length;
		return true;
	}

	return false;
}