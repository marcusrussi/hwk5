#ifndef _stack_h
#define _stack_h

// Stack.h                                      Stan Eisenstat (02/23/14)
//
// Define the abstract data type for a Stack of string pointers.  The strings
// themselves are NOT stored.

// Define true and false

#include <stdbool.h>


// A variable of type Stack is a pointer to the struct used to implement the
// stack operations declared below.  Note that the declaration of that struct
// appears only in the file that implements these operations.  Thus the calling
// program does not know what fields have been defined and cannot access them;
// and the variable must be a pointer since the size of the struct is unknown.

typedef struct stack *Stack;


// The following functions are the only means of accessing a Stack data
// structure.  Each requires a pointer to a Stack variable in case it needs
// to modify the value of that variable (or more generally for consistency).
// Each returns the status of the operation, either true (= success) or false
// (= failure; e.g., an invalid argument, an inconsistent Stack object, or a
// NULL return from malloc() / realloc()).


// Set *STK to a new object of type Stack.  Return true if successful, false
// otherwise.
int createS (Stack *stk);


// Return true if the Stack *STK is empty, false otherwise.  The value of *STK
// may change as a result.
int isEmptyS (Stack *stk);


// Push the string pointer S on the top of the Stack *STK; the string itself
// is not copied.  The value of *STK may change as a result.  Return true if
// successful, false otherwise.
int pushS (Stack *stk, char *s);


// Remove the string pointer on the top of the Stack *STK and store that value
// in *S.  The value of *STK may change as a result.  Return true if
// successful, false otherwise (e.g., if *STK is empty).
int popS (Stack *stk, char **s);


// Store in *S the value of the string pointer on the top of the Stack *STK.
// The value of *STK may change as a result.  Return true if successful, false
// otherwise (e.g., if *STK is empty).
int topS (Stack *stk, char **s);


// Destroy the Stack *STK by freeing any storage that it uses (but not the
// blocks of storage to which the string pointers point) and set the value of
// *STK to NULL.  Return true if successful, false otherwise.
int destroyS (Stack *stk);

#endif
