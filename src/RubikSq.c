#include <stdio.h>
#include "dbg.h"

#include "RubikSq_args.h"
#include "utils.h"
#include "Queue.h"
#include "Trie.h"

#define strEquals(a, b) (strcmp( (a), (b) ) == 0)

int main(int argc, char *argv[])
{
    Config *args = RubikSq_args( argc, argv );
    check( args != NULL, "Error in argument processing." );

    Trie dictionary;
    Queue queue;

    int r;
    r = createT( &dictionary );
    check( r, "Trie creation failed." );
    r = createQ( &queue );
    check( r, "Queue creation failed." );

    r = insertT( &dictionary, args->goal, "", 0 );
    check( r, "Failed to insert first value into 'dictionary'." );

    char *from; int len;
    r = searchT( &dictionary, args->goal, &from, &len );
    check( r, "whataaaaa" );

    r = pushQ( &queue, args->goal );

    while( !isEmptyQ( &queue ) ) {
        char *pos, *from;
        int len;

        r = popQ( &queue, &pos );
        check( r, "Queue empty, but should have had at least 1 item in it." );

        r = searchT( &dictionary, pos, &from, &len );
        check( r, "'%s' not found in dictionary, but should be there.", pos );

        if( len < args->maxlength ) {
            int nperms;
            char **perms = allPermutations( pos, &nperms, args->general, \
                                            args->height, args->width );
            check( perms != NULL, "allPermutations returned NULL." );

            for( int i=0; i < nperms; i++ ) {
                char *s;
                int *t;

                if( strEquals( args->initial, perms[i] ) ) {
                    r = insertT( &dictionary, perms[i], pos, len + 1 ) ;
                    check( r, "Trie insert failed." );

                    printSolution( args->initial, dictionary );
                    return EXIT_SUCCESS;
                } else if ( !searchT( &dictionary, perms[i], &s, &t ) ) {
                    r = insertT( &dictionary, perms[i], pos, len + 1 ) ;
                    check( r, "Trie insert failed." );
                    r = pushQ( &queue, perms[i] );
                }
            }
        }
    }

    free( args );
    return 0;

error:
	free( args );
	exit( EXIT_FAILURE );
}
