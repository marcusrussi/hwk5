#ifndef _rubiksq_args_h
#define _rubiksq_args_h

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "dbg.h"

#define strEquals(a, b) (strcmp( (a), (b) ) == 0)
#define NUMCHARS 12

typedef struct Config {
    bool general;
    bool dimensions;

    int height;
    int width;

    int maxlength;
    char *initial;
    char *goal;
} Config;

Config *RubikSq_args( int argc, char **argv );

#endif
