#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"


// The internal representation of the Stack data type is a pointer to the
// header node of a headed, singly-linked list.

typedef struct stackItem_ {
  char *value;
  struct stackItem_ *next;
} stackItem;

struct stack {
  stackItem *first;
};


// Implementation of createS()
int createS (Stack *stk)
{
  // Allocate space for a new STACK struct
  struct stack *tmp = malloc (sizeof(struct stack));

  if (tmp != NULL) {
    // Set the head's pointer to the first element of the 
    // linked list to be NULL
    tmp->first = NULL;

    // Point the stack pointer in the right direction
    *stk = tmp;
    return true;
  }

  // If malloc() had an error, return false
  return false;
}


// Implementation of isEmptyS()
int isEmptyS (Stack *stk)
{
  if ( (*stk)->first == NULL )
    return true;
  return false;
}


/**
 * Implementation of pushS
 * @param  stk pointer to a Stack (which itself is a pointer)
 * @param  s   string pointer to be pushed on the stack
 * @return     returns 'true' on success, 'false' on failure
 */
int pushS (Stack *stk, char *s)
{
  stackItem *tmp = malloc (sizeof (stackItem));

  if (tmp == NULL)
    return false;

  // If the stack is empty
  if ( (*stk)->first == NULL ) {
    if (tmp == NULL) {
      free (tmp);
      return false;
    }

    tmp->value = s;
    tmp->next  = NULL;

    (*stk)->first = tmp;

    return true;
  }

  stackItem *pointerToOldHead = (*stk)->first;

  tmp->next  = pointerToOldHead;
  tmp->value = s;

  (*stk)->first = tmp;

  return true;
}


// Implementation of popS()
int popS (Stack *stk, char **s)
{
  // Check to make sure the stack has at least one item in it
  if (isEmptyS(stk))
    return false;

  stackItem *newHead = (*stk)->first->next;
  *s = (*stk)->first->value;

  free ((*stk)->first);

  (*stk)->first = newHead;

  return true;
}


// Implementation of topS()
int topS (Stack *stk, char **s)
{
  if (isEmptyS(stk))
    return false;

  *s = (*stk)->first->value;

  return true;
}


// Implementation of destroyS()
int destroyS (Stack *stk)
{
  if (isEmptyS(stk)) {
    free (*stk);
    return true;
  }

  stackItem *nextStackItem = (*stk)->first;

  while (nextStackItem != NULL) {
    void *tmp = nextStackItem;
    nextStackItem  = nextStackItem->next;

    free(tmp);
  }

  free (*stk);

  return true;
}
