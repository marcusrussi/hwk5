#include "RubikSq_args.h"

/**
 * Converts a null-terminated string to an integer.
 * The string specified must be a sequence of decimal digits, and
 * must be nonnegative.
 * @param  str The string to be processed
 * @return     On success, returns the integer.  On failure, returns -1.
 */
int strtoint( char *str );

Config *RubikSq_args( int argc, char **argv )
{
    Config *tmp = malloc( sizeof(Config) );

    check( argc > 3 || argc < 8, \
           "usage: RubikSq [-r] [HEIGHT WIDTH] MAXLENGTH INITIAL GOAL");

    int n = 1;
    tmp->general    = false;
    tmp->dimensions = false;
    tmp->height     = 3;
    tmp->width      = 3;

    if( argc == 3 + 1 )
        ;
    else if( argc == 4 + 1 ) {
        n++;
        tmp->general = true;
    } else if( argc == 5 + 1 ) {
        tmp->dimensions = true;

        tmp->height = strtoint( argv[n++] );
        tmp->width  = strtoint( argv[n++] );
    } else if( argc == 6 + 1 ) {
        n++;
        tmp->general = true;
        tmp->dimensions = true;

        tmp->height = strtoint( argv[n++] );
        tmp->width  = strtoint( argv[n++] );
    } else {
        sentinel("'argc' was %d long.  Should be 4, 5, 6, or 7.", argc );
    }

    tmp->maxlength = strtoint( argv[n++] );
    tmp->initial   = argv[n++];
    tmp->goal      = argv[n++];

    if( tmp->general == true )
        check( strEquals( argv[1], "-r" ), \
               "'-r' flag incorrectly specified as '%s'.", argv[1] );
    check( tmp->height > -1, "HEIGHT incorrectly specified." );
    check( tmp->width > -1, "WIDTH incorrectly specified." );
    check( tmp->maxlength > -1, "MAXLENGTH incorrectly specified.");

    check( 2 <= tmp->height && tmp->height <= 5, \
           "HEIGHT was set to %d, but must be an integer between 2 and 5.", \
           tmp->height );

    check( 2 <= tmp->width && tmp->width <= 5, \
           "WIDTH was set to %d, but must be an integer between 2 and 5.", \
           tmp->width );

    check( tmp->maxlength > -1, "MAXLENGTH was specified as %d, but it " \
           "must be a nonnegative integer.", tmp->maxlength );

    int l, m;
    check( (l = strlen( tmp->initial )) == (m = tmp->width * tmp->height), \
           "INITIAL has length %d but must have length %d * %d = %d.", \
           l, tmp->width, tmp->height, m );

    check( (l = strlen( tmp->goal )) == m, \
           "GOAL has length %d but must have length %d * %d = %d.", \
           l, tmp->width, tmp->height, m );

    int chars_Initial[NUMCHARS], chars_Goal[NUMCHARS];

    for( int i=0; i < NUMCHARS; i++ ) {
        chars_Initial[i] = 0;
        chars_Goal[i]    = 0;
    }

    for( int i=0; i < l; i++ ) {
        int q = tmp->initial[i] - 'A';
        int r = tmp->goal[i] - 'A';

        chars_Initial[q]++;
        chars_Goal[r]++;

        check( 0 <= q && q <= 11, \
               "INITIAL must be a char between A and L, but was given as '%c'",\
               tmp->initial[i] );
        check( 0 <= r && r <= 11, \
               "GOAL must be a char between A and L, but was given as '%c'",\
               tmp->goal[i] );
    }

    for( int i=0; i < NUMCHARS; i++ )
        check( chars_Goal[i] == chars_Initial[i], \
               "There were %d instances of %c in INITIAL, but in GOAL, there " \
               "were %d instances.", chars_Initial[i], i + 'A', chars_Goal[i] );

    return tmp;

error:
    free( tmp );
    return NULL;
}

int strtoint( char *str )
{
    check_mem( str );

    int number = 0, c;
    int length = strlen( str );

    check( length > 0, "Length of the string must be greater than 0." );

    for( int i=0; i < length; i++ ) {
        c = str[i] - '0';
        check( 0 <= c && c <= 9, \
               "'%c' is not a decimal digit (0-9 only).", str[i] );
        number *= 10;
        number += c;
    }

    return number;

error:
    return -1;
}
