#include <string.h>
#include <stdbool.h>
#include "minunit.h"
#include "../src/Trie.h"
#include "../src/Trie.c"
#include "../src/utils.h"
#include "../src/utils.c"

#define strEquals(a, b) (strcmp( (a), (b) ) == 0)

char *test_create()
{
	Trie test1, test2;
	mu_assert( createT( &test1 ), "Trie failed to be created." );
	mu_assert( createT( &test2 ), "Trie failed to be created." );
	mu_assert( test1 != test2, "Two tries pointed to the same thing." );

	return NULL;
}

char *test_basic()
{
	Trie test;
	createT( &test );

	int nInserts = 6;
	char *poss[] = {"A", "AB", "ABC", "ABCDEFGHI", "DCA", "C"};
	char *froms[] = {"B", "BA", "BCA", "BCADEFGHI", "CAD", "C"};
	char *lengths[] = { 1, 2, 3, 4, 5, 6 };

	for( int i=0; i < nInserts; i++) {
		mu_assert( insertT( &test, poss[i], froms[i], lengths[i] ), \
			"Insert failed." );
	}

	for( int i=0; i < nInserts; i++ ) {
		char *pos = poss[i];
		char *from = froms[i];
		char *fromr;
		int len = lengths[i];
		int lenr;

		mu_assert( searchT( &test, pos, &fromr, &lenr ), "Search failed." );
		mu_assert( strEquals( from, fromr ), \
			"The 'from' that was inserted ≠ what was extracted." );
		mu_assert( len == lenr, "The LEN inserted ≠ what was extracted." );
	}

	return NULL;
}

char *test_reallybasic()
{
	Trie dictionary;
	mu_assert( createT( &dictionary ), "Trie failed to be created." );

	mu_assert( insertT( &dictionary, "ABCDEFGHI", "", 0 ), "Insert failed." );

	char *from;
	int len;
	mu_assert( searchT( &dictionary, "ABCDEFGHI", &from, &len ), \
		"Search for 'ABCDEFGHI' failed." );

	mu_assert( strEquals( from, "" ) , "NULL ≠ NULL." );
	mu_assert( len == 0, "Length should be 0." );

	return NULL;
}

char *test_duplicate()
{
	Trie test;
	createT( &test );

	mu_assert( insertT( &test, "ABC", "BCA", 5 ), "First insert failed." );
	mu_assert( insertT( &test, "ABC", "BCA", 5 ), "Duplicate insert failed." );

	return NULL;
}

char *test_heavy()
{
	Trie test;
	createT( &test );

	char *cube = "ABCDEFGHIABCDEFGHIABCDEFGHIABCDEFGHI" \
				 "AGDIIHEEFAGDIIHEEFAGDIIHEEFAGDIIHEEF" \
				 "ADBEIHGFDADBEIHGFDADBEIHGFDADBEIHGFD" \
				 "ADBCEIFHDADBCEIFHDADBCEIFHDADBCEIFHD" \
				 "AAAAABBBBAAAAABBBBAAAAABBBBAAAAABBBB" \
				 "GGGIHHHHDGGGIHHHHDGGGIHHHHDGGGIHHHHD";

	int nperms;
	int len = 1;
	char **perms = allPermutations( cube, &nperms, 1, 6, 36 );
	debug( "nperms: %d", nperms );

	for( int i=0; i < nperms; i++ ) {
		mu_assert( insertT( &test, perms[i], cube, len ), "Insert failed." );
	}

	for( int i=0; i < nperms; i++ ) {
		char *pos = perms[i];
		char *from = cube;
		char *fromr;
		int lenr;

		mu_assert( searchT( &test, pos, &fromr, &lenr ), "Search failed." );
		mu_assert( strEquals( from, fromr ), \
			"The 'from' that was inserted ≠ what was extracted." );
		mu_assert( len == lenr, "The LEN inserted ≠ what was extracted." );
	}

	return NULL;
}

char *all_tests()
{
	mu_suite_start();

	mu_run_test( test_create );
	mu_run_test( test_basic );
	mu_run_test( test_reallybasic );
	mu_run_test( test_duplicate );
	mu_run_test( test_heavy );

	return NULL;
}

RUN_TESTS( all_tests )
