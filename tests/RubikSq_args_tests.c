#include "minunit.h"
#include "../src/RubikSq_args.h"
#include "../src/RubikSq_args.c"

char *test_terrible_args()
{
    int c = 3;
    char *args1[3] = {"lol", "so", "bad"};

    Config *result = RubikSq_args( c, args1 );

   	mu_assert( result == NULL, \
   			   "Only passing three arguments must result in failure." );
    free( result );

    c = 8;
    char *args2[8] = {"lol", "so", "bad", "lol", "lol", "lol", "lol", "lol"};

    result = RubikSq_args( c, args2 );

    mu_assert( result == NULL, \
           "Passing more than 7 arguments must result in failure." );
    free( result );
    return NULL;
}

char *test_invalid_types()
{
    /* -----------------------------------------------------------------------*/
    int c = 4;
    char *args1[4] = {"RubikSq", "15 ", "ABCDEFDEF", "ABCDEFDEF"};
    Config *result = RubikSq_args( c, args1 );

    mu_assert( result == NULL, "'15 ' isn't a valid number"  );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 4;
    char *args2[4] = {"RubikSq", "-r", "ABCDEFDEF", "ABCDEFDEF"};
    result = RubikSq_args( c, args2 );

    mu_assert( result == NULL, "'-r' should have been a number" );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 6;
    char *args3[6] = {"RubikSq", "-r", "03 ", "ABCDEFDEF", "ABCDEFDEF"};
    result = RubikSq_args( c, args3 );

    mu_assert( result == NULL, "'03 ' isn't valid" );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 5;
    char *args4[5] = {"RubikSq", "-r", "01000." "ABCDEFDEF", "ABCDEFDEF"};
    result = RubikSq_args( c, args4 );

    mu_assert( result == NULL, "'01000.' has an illegal decimal point" );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 5;
    char *args5[5] = {"RubikSq", "", "3", "ABCDEFDEF", "ABCDEFDEF"};
    result = RubikSq_args( c, args5 );

    mu_assert( result == NULL, \
      "'' blank string for HEIGHT or WIDTH = illegal" );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 7;
    char *args6[7] = {"RubikSq", "-r", "0000002", "2", "-100", "ABCD", "ABCD"};
    result = RubikSq_args( c, args6 );

    mu_assert( result == NULL, "'-100' is invalid." );
    free( result );

    return NULL;
}

char *test_cubes()
{
    /* -----------------------------------------------------------------------*/
    int c = 4;
    char *args1[4] = {"RubikSq", "0", "ABCDEFDEF", "ABCDEFDEF"};
    Config *result = RubikSq_args( c, args1 );

    mu_assert( result != NULL, "Strings are equal, and must be accepted" );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 4;
    char *args2[4] = {"RubikSq", "0", "ABBBBB", "BBBBBA"};
    result = RubikSq_args( c, args2 );

    mu_assert( result == NULL, "'ABBBBB' and 'BBBBBA' are VALID." );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 4;
    char *args3[4] = {"RubikSq", "0", "ABCD", "ABCDE"};
    result = RubikSq_args( c, args3 );

    mu_assert( result == NULL, \
      "INITIAL and GOAL have different lengths (illegal)" );
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 4;
    char *args4[4] = {"RubikSq", "0", "ABAB", "ABABC"};
    result = RubikSq_args( c, args4 );

    mu_assert( result == NULL, "Different lengths, must be illegal.");
    free( result );

    /* -----------------------------------------------------------------------*/
    c = 4;
    char *args5[4] = {"RubikSq", "0", "ABABC", "ABCCC"};
    result = RubikSq_args( c, args5 );

    mu_assert( result == NULL, \
      "'ABABC' and 'ABCCC' is illegal because of duplicates" );
    free( result );

    return NULL;
}

char *all_tests()
{
    mu_suite_start();

    mu_run_test( test_terrible_args );
    mu_run_test( test_invalid_types );
    mu_run_test( test_cubes );

    return NULL;
}

RUN_TESTS( all_tests )
