#include <string.h>
#include <stdbool.h>
#include "minunit.h"
#include "../src/utils.h"
#include "../src/utils.c"
#include "../src/Trie.h"
#include "../src/Trie.c"

#define strEquals(a, b) (strcmp( (a), (b) ) == 0)

char *cube2 = "A";

char *cube3 = "1" \
			  "2" \
			  "3" \
			  "4" \
			  "5" \
			  "6";

char *cube4 = "abcde" \
			  "fghij" \
			  "klmno" \
			  "pqrst" \
			  "uvwxy";

char *test_cube1()
{
	char *cube1 = "ABC" \
			  	  "DEF" \
				  "GHI" \
			      "JKL";

	char *r = shift( cube1, DOWN, 1, 0, 4, 3 );
	mu_assert( r != NULL, "shift() should not have returned false." );
	mu_assert( strEquals( r, "JBCAEFDHIGKL" ), "shift() result incorrect");

	r = shift( r, RIGHT, 1, 0, 4, 3 );
	mu_assert( r != NULL, "shift() should not have returned false." );
	mu_assert( strEquals( r, "CJBAEFDHIGKL" ), "shift() result incorrect" );

	r = shift( r, RIGHT, 2, 1, 4, 3 );
	mu_assert( r != NULL, "shift() should not have returned false." );
	mu_assert( strEquals( r, "CJBEFADHIGKL" ), "shift() result incorrect" );

	r = shift( r, RIGHT, 3, 2, 4, 3 );
	mu_assert( r != NULL, "shift() should not have returned false." );
	mu_assert( strEquals( r, "CJBEFADHIGKL" ), "shift() result incorrect" );

	r = shift( r, DOWN, 4, 2, 4, 3 );
	mu_assert( r != NULL, "shift() should not have returned false." );
	mu_assert( strEquals( r, "CJBEFADHIGKL" ), "shift() result incorrect" );

	return NULL;
}

char *test_cube2()
{
	char *cube = "AB" \
				 "CD";

	char *r = shift( cube, RIGHT, 1, 0, 2, 2 );
	mu_assert( r != NULL, "shift() should not have returned false." );
	mu_assert( strEquals( r, "BACD" ), "shift() result incorrect" );

	r = shift( r, RIGHT, 2, 2, 2, 2 );
	mu_assert( r == NULL, "shift() should have returned false." );

	return NULL;
}

char *test_printSolution()
{
	Trie test;
	createT( &test );
	insertT( &test, "A", "", 0 );
	insertT( &test, "B", "A", 0 );
	insertT( &test, "C", "B", 0 );

	printSolution( "C", test );
	// Should output:
	// C
	// B
	// A

	return NULL;
}

char *all_tests()
{
	mu_suite_start();

	mu_run_test( test_cube1 );
	mu_run_test( test_cube2 );
	mu_run_test( test_printSolution );

	return NULL;
}

RUN_TESTS( all_tests )
