#include "minunit.h"
#include "../src/RubikSq_args.h"
#include "../src/RubikSq_args.c"

char *test_whitespace()
{
    int result = strtoint( "" );
   	mu_assert( result == -1, "Empty string must cause failure" );

    result = strtoint( "   " );
    mu_assert( result == -1, "Whitespace must cause failure" );

    result = strtoint( "    3" );
    mu_assert( result == -1, \
           "Empty string must cause failure, even when there's a number" );

    result = strtoint( "3   " );
    mu_assert( result == -1, "Whitespace after number must cause failure" );

    result = strtoint( "3   " );
    mu_assert( result == -1, "Whitespace after number must cause failure" );

    return NULL;
}

char *test_sanity()
{
    int result = strtoint( "34" );
    mu_assert( result == 34, "'34' -> 34" );

    result = strtoint( "0" );
    mu_assert( result == 0, "'0' -> 0" );

    result = strtoint( "5" );
    mu_assert( result == 5, \
           "'5' -> 5" );

    result = strtoint( "1000" );
    mu_assert( result == 1000, "'1000' -> 1000" );

    result = strtoint( "1" );
    mu_assert( result == 1, "'1' -> 1" );

    return NULL;
}

char *test_hardones()
{
    int result = strtoint( "0000000034" );
    mu_assert( result == 34, "'0000000034' -> 34" );

    result = strtoint( "34000" );
    mu_assert( result == 34000, "'34000' -> 34000" );

    result = strtoint( "10101" );
    mu_assert( result == 10101, "'10101' -> 10101" );

    result = strtoint( "00000" );
    mu_assert( result == 0, "'00000' -> 0" );

    result = strtoint( "123454321" );
    mu_assert( result == 123454321, "'123454321' -> 123454321" );

    return NULL;
}

char *test_fail_1()
{
    int result = strtoint( "34.1" );
    mu_assert( result == -1, "'34.1' must cause failure" );

    result = strtoint( "00." );
    mu_assert( result == -1, "'00.' must cause failure" );

    result = strtoint( "00\n" );
    mu_assert( result == -1, "'00\\n' must cause failure" );

    result = strtoint( "00\0" );
    mu_assert( result == 0, "'00\\0 must cause failure'" );

    result = strtoint( "123454321" );
    mu_assert( result == 123454321, "Whitespace after number must cause failure" );

    return NULL;
}

char *test_fail_2()
{
    int result = strtoint( "!!!!!" );
    mu_assert( result == -1, "'!!!!!' must cause failure" );

    result = strtoint( "1/" );
    mu_assert( result == -1, "'1/'' must cause failure" );

    result = strtoint( "0:" );
    mu_assert( result == -1, "'0:' must cause failure" );

    result = strtoint( "abcde" );
    mu_assert( result == -1, "'abcde' must cause failure'" );

    return NULL;
}

char *all_tests()
{
    mu_suite_start();

    mu_run_test( test_whitespace );
    mu_run_test( test_sanity );
    mu_run_test( test_hardones );
    mu_run_test( test_fail_1 );
    mu_run_test( test_fail_2 );

    return NULL;
}

RUN_TESTS( all_tests )
